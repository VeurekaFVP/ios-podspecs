Pod::Spec.new do |s|

  s.name         = "AlamofireProxy"
  s.version      = "1.0.2"
  s.summary      = "Libreria para uso de Proxy y Router de Alamofire."
  s.description      = 'Esta Libreria proporciona una API para manejo de Alamofire proporcionando la estructura necesaria para crear un Router para peticiones a servicios REST y operaciones basicas.'

  s.homepage         = 'http://www.veureka.com/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-alamofireproxy.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.0' }

  s.source_files = [
    'Classes/**/*',
  ]

  s.dependency 'CommonsAssets','1.0.3'
  s.dependency 'Logger','1.0.3'
  s.dependency 'Alamofire','4.6.0'
  s.dependency 'SwiftyJSON','3.1.4'

end
