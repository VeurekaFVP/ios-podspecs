Pod::Spec.new do |s|

	s.name         = "BundleManager"
	s.version      = "1.0.0"
	s.summary      = "Libreria para API Manejo de Archivos PLIST."
	s.description      = 'Esta Libreria proporciona una API para manejo de las propiedades en los archivos PLIST.'

	s.homepage         = 'http://veureka.net/'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
	s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-bundlemanager.git', :tag => s.version.to_s }

	s.ios.deployment_target = '8.0'

	s.source_files = [
		'Classes/**/*',
	]

    s.dependency 'CommonsAssets','1.0.0'
end
