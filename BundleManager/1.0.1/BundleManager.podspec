Pod::Spec.new do |s|

	s.name         = "BundleManager"
	s.version      = "1.0.1"
	s.summary      = "Libreria para API Manejo de Archivos PLIST."
	s.description      = 'Esta Libreria proporciona una API para manejo de las propiedades en los archivos PLIST.'

	s.homepage         = 'http://www.veureka.com/'
	s.license          = { :type => 'MIT', :file => 'LICENSE' }
	s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
	s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-bundlemanager.git', :tag => s.version.to_s }

	s.source_files = [
		'Classes/**/*',
	]

    s.platform         = :ios, '9.0'
    s.requires_arc     = true
    s.static_framework = true
    s.dependency "Firebase/RemoteConfig"
    s.dependency 'CommonsAssets','1.0.3'

end
