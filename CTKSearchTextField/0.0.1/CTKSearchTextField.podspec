Pod::Spec.new do |s|

  s.name         = "CTKSearchTextField"
  s.version      = "0.0.1"
  s.summary      = "TextField con lista de búsqueda"

  s.description  = <<-DESC
CTKFlagPhoneNumber is a phone number textfield that allows you to choose the country code thanks to a picker. It uses libPhoneNumber to format the number in the textfield according to country code.
CTKMultimediaPicker is a wrapper built around UIImagePickerController and UIDocumentPickerViewController.
  DESC

  s.homepage     = 'http://www.veureka.com/'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/jrvillalobos/ctksearchtextfield.git', :tag => s.version.to_s }

  s.platform         = :ios, '9.0'
  s.requires_arc     = true
  s.static_framework = true

  s.source_files = 'Source/CTKSearchTextField/**/*.swift'

  s.dependency 'CommonsAssets','1.0.3'

end

