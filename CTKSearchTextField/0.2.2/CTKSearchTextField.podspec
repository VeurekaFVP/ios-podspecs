#
#  Be sure to run `pod spec lint CTKSearchTextField.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "CTKSearchTextField"
  spec.version      = "0.2.2"
  spec.summary      = "TextField con lista de búsqueda"

  spec.description  = <<-DESC
  CTKFlagPhoneNumber is a phone number textfield that allows you to choose the country code thanks to a picker. It uses libPhoneNumber to format the number in the textfield according to country code.
  CTKMultimediaPicker is a wrapper built around UIImagePickerController and UIDocumentPickerViewController.
  DESC

  spec.homepage     = "https://bitbucket.org/jrvillalobosm"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author       = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  spec.platform     = :ios, '9.0'

  spec.source       = { :git => "https://bitbucket.org/jrvillalobosm/ios-utilities.git", :tag => "CTKSearchTextField_0.2.1" }

  spec.source_files = 'Sources/CTKSearchTextField/**/*.swift'

  spec.frameworks   = 'UIKit'

  spec.dependency 'CommonsAssets','~> 2.1.0'
end
