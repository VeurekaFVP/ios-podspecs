Pod::Spec.new do |s|

  s.name         = "CommonsAssets"
  s.version      = "1.0.0"
  s.summary      = "Librería para utilerías."

  s.description  = 'Esta librería proporciona Utilerias comunes'

  s.homepage     = 'http://veureka.net/'

  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }


  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-commonsassets.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = [
    'Classes/**/*',
  ]

  s.frameworks = 'UIKit'
  s.dependency 'ObjectMapper','2.2.6'
  s.dependency 'RxSwift','3.4.1'

end
