Pod::Spec.new do |s|

  s.name         = "CommonsAssets"
  s.version      = "2.0.0"
  s.summary      = "Librería para utilerías."

  s.description  = 'Esta librería proporciona Utilerias comunes: BundleManager, Extensiones de datos, Validaciones, Utilerias, Extensiones UIKit'

  s.homepage     = 'http://www.veureka.com/'

  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }


  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-commonsassets.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = [
    'Classes/**/*',
  ]

  s.frameworks = 'UIKit'
  s.dependency 'ObjectMapper','3.3.0'
  s.dependency 'RxSwift','3.6.1'

end
