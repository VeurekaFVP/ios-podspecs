Pod::Spec.new do |spec|

  spec.name         = "CommonsAssets"
  spec.version      = "2.2.0"
  spec.summary      = "Librería para utilerías."

  spec.description  = <<-DESC
    Esta librería proporciona Utilerias comunes: BundleManager, Extensiones de datos, Validaciones, Utilerias, Extensiones UIKit
  DESC

  spec.homepage     = "https://bitbucket.org/jrvillalobosm"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author       = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  spec.platform     = :ios, '11.0'

  spec.swift_versions = ['4.2', '5.0']
  
  spec.ios.vendored_frameworks = 'CommonsAssets.framework'
  spec.source       = { :http => "https://bitbucket.org/jrvillalobosm/cocoapods-frameworks/raw/023cc31/CommonsAssets.zip" }

  spec.frameworks = 'UIKit'

  spec.dependency 'ObjectMapper','3.5.1'
  spec.dependency 'RxSwift','4.5.0'
  spec.dependency 'Validator', '3.2.1'
end
