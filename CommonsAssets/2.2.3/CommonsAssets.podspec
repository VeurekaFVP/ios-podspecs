#
#  Be sure to run `pod spec lint CommonsAssets.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "CommonsAssets"
  spec.version      = "2.2.3"
  spec.summary      = "Librería para utilerías."

  spec.description  = <<-DESC
    Esta librería proporciona Utilerias comunes: BundleManager, Extensiones de datos, Validaciones, Utilerias, Extensiones UIKit
  DESC

  spec.homepage     = "https://bitbucket.org/jrvillalobosm"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author       = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  spec.platform     = :ios, '11.0'

  spec.source       = { :git => "https://bitbucket.org/jrvillalobosm/ios-utilities.git", :tag => "CommonsAssets_2.2.3" }

  spec.source_files = 'Sources/CommonsAssets/**/*.swift'

  spec.swift_versions = ['4.2', '5.0']

  spec.frameworks = 'UIKit'

  spec.dependency 'ObjectMapper','3.5.1'
  spec.dependency 'RxSwift', '~> 5.0'
  spec.dependency 'Validator', '3.2.1'
end
