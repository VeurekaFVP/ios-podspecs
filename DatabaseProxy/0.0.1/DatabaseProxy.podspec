Pod::Spec.new do |s|

  s.name         = "DatabaseProxy"
  s.version      = "0.0.1"
  s.summary      = "A short description of DatabaseProxy."

  s.description  = <<-DESC
  Esta Librería cuenta con métodos de Firebase como observables haciendo uso de RxSwift, así como envolventes para hacer uso directo de Firebase sin importar estas librerías.
  También cuenta con API para autenticar con Facebook.
                   DESC

  s.homepage     = 'http://www.veureka.com/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  s.source           = { :git => 'https://bitbucket.org/jrvillalobosm/ios-database-proxy.git', :tag => s.version.to_s }

  s.source_files = [
    'Source/**/*',
  ]

  s.platform         = :ios, '9.0'
  s.requires_arc     = true
  s.static_framework = true
  s.dependency "Firebase/RemoteConfig", "5.5.0"
  s.dependency "Firebase/Storage", "5.5.0"
  s.dependency 'CommonsAssets','~> 2.0.0'

end
