Pod::Spec.new do |s|

  s.name         = "DatabaseProxy"
  s.version      = "0.1.1"
  s.summary      = "API para manejo de base de datos Server y Local."

  s.description  = <<-DESC
  Esta Librería cuenta con métodos de Firebase como observables haciendo uso de RxSwift, así como envolventes para hacer uso directo de Firebase sin importar estas librerías.
  También cuenta con API para autenticar con Facebook.
                   DESC

  s.homepage     = 'http://www.veureka.com/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  s.source           = { :git => 'https://bitbucket.org/jrvillalobosm/ios-database-proxy.git', :tag => "0.1.0" }

  s.source_files = [
    'Sources/**/*',
  ]

  s.platform         = :ios, '9.0'
  s.requires_arc     = true
  s.static_framework = true
  s.dependency "Firebase/RemoteConfig", "5.20.2"
  s.dependency "Firebase/Storage", "5.20.2"
  s.dependency "Firebase/Auth", "5.20.2"
  s.dependency "Firebase/Database", "5.20.2"
  s.dependency "CommonsAssets", "~> 2.1.2"
  s.dependency "HttpNetworkLayer", "~> 2.1.0"
  s.dependency "FacebookCore", "0.5.0"
  s.dependency "FacebookLogin", "0.5.0"
end
