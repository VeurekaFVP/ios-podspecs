Pod::Spec.new do |s|

  s.name         = "GoogleMapsProxy"
  s.version      = "1.0.0"
  s.summary      = "Libreria para uso de Proxy y Router de GoogleMapsProxy."

  s.description  = 'Esta libreria proporciona una API para manejo de GoogleMaps proporcionando conexion a servicios REST de Google'

  s.homepage         = 'http://veureka.net/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-googlemapsproxy.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = [
    'Classes/**/*',
  ]

  s.dependency 'CommonsAssets','1.0.0'
  s.dependency 'BundleManager','1.0.0'
  s.dependency 'Logger','1.0.0'
  s.dependency 'AlamofireProxy','1.0.0'
  s.dependency 'GooglePlacesAPI','1.1.3'
  s.dependency 'GoogleMapsDirections','1.1.3'

end
