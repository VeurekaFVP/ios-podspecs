Pod::Spec.new do |s|

  s.name         = "GoogleMapsProxy"
  s.version      = "1.0.3"
  s.summary      = "Libreria para uso de Proxy y Router de GoogleMapsProxy."

  s.description  = 'Esta libreria proporciona una API para manejo de GoogleMaps proporcionando conexion a servicios REST de Google. Actualmente cuenta con servicio para lugares y obtener direcciones'

  s.homepage         = 'http://www.veureka.com/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-googlemapsproxy.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.0' }

  s.source_files = [
    'Classes/**/*',
  ]

  s.dependency 'CommonsAssets','1.0.3'
  s.dependency 'Logger','1.0.3'
  s.dependency 'AlamofireProxy','1.0.2'

end
