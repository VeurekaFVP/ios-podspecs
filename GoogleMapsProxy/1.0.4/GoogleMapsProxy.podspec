Pod::Spec.new do |s|

  s.name         = "GoogleMapsProxy"
  s.version      = "1.0.4"
  s.summary      = "Libreria para uso de Proxy y Router de GoogleMapsProxy."

  s.description  = 'Esta libreria proporciona una API para manejo de GoogleMaps proporcionando conexion a servicios REST de Google. Actualmente cuenta con servicio para lugares y obtener direcciones'

  s.homepage         = 'http://www.veureka.com/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-googlemapsproxy.git', :tag => s.version.to_s }

  s.source_files = [
    'Classes/**/*',
  ]

  s.platform         = :ios, '9.0'
  s.requires_arc     = true
  s.static_framework = true
  s.dependency 'BundleManager','1.0.1'
  s.dependency 'CommonsAssets','1.0.3'
  s.dependency 'Logger','1.0.3'
  s.dependency 'AlamofireProxy','1.0.2'

end
