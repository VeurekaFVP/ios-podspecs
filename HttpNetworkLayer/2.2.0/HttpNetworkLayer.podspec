#
#  Be sure to run `pod spec lint HttpNetworkLayer.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "HttpNetworkLayer"
  spec.version      = "2.2.0"
  spec.summary      = "Libreria para uso de Proxy y Router de Alamofire."

  spec.description  = <<-DESC
  Esta Libreria proporciona una API para manejo de Alamofire proporcionando la estructura necesaria para crear un Router para peticiones a servicios REST y operaciones basicas.
  DESC

  spec.homepage     = "https://bitbucket.org/jrvillalobosm"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author       = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  spec.platform     = :ios, '11.0'

  spec.source       = { :git => "https://bitbucket.org/jrvillalobosm/ios-network-layer.git", :tag => "HttpNetworkLayer_2.2.0" }

  spec.swift_versions = ['4.2', '5.0']

  spec.source_files = 'Sources/HttpNetworkLayer/**/*.swift'

  spec.dependency 'Logger','~> 2.2.0'
  spec.dependency 'Alamofire','~> 4.9.0'
  spec.dependency 'SwiftyJSON','5.0.0'
end
