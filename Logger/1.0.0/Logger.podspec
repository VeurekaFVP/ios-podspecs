Pod::Spec.new do |s|

  s.name         = "Logger"
  s.version      = "1.0.0"

  s.summary      = "Libreria para API de Logs basado en Log4j."
  s.description      = 'Esta Libreria proporciona una API para manejo de logs con Crash Reporting.'

  s.homepage         = 'http://veureka.net/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-logger.git', :tag => s.version.to_s }

  s.ios.deployment_target = '8.0'

  s.source_files = [
    'Classes/**/*',
  ]

  s.resources = ['Resources/**/*']

  s.dependency 'BundleManager','1.0.0'
  s.dependency 'CommonsAssets','1.0.0'

  s.frameworks = 'UIKit'

end
