#
#  Be sure to run `pod spec lint MapsEngine.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "MapsEngine"
  spec.version      = "2.1.2"
  spec.summary      = "Libreria para uso de Proxy y Router de MapsEngine."

  spec.description  = <<-DESC
    Esta libreria proporciona una API para manejo de GoogleMaps proporcionando conexion a servicios REST de Google. Actualmente cuenta con servicio para lugares y obtener direcciones
  DESC

  spec.homepage     = "https://bitbucket.org/jrvillalobosm"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author       = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  spec.platform     = :ios, '9.0'

  spec.source       = { :git => "https://bitbucket.org/jrvillalobosm/ios-network-layer.git", :tag => "MapsEngine_2.1.1" }

  spec.source_files = 'Sources/MapsEngine/**/*.swift'

  spec.requires_arc = true
  spec.static_framework = true
  spec.dependency 'DatabaseProxy','~> 0.1.1'
end
