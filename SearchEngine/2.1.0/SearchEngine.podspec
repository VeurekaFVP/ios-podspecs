#
#  Be sure to run `pod spec lint SearchEngine.podspec' to ensure this is a
#  valid spec and to remove all comments including this before submitting the spec.
#
#  To learn more about Podspec attributes see https://docs.cocoapods.org/specification.html
#  To see working Podspecs in the CocoaPods repo see https://github.com/CocoaPods/Specs/
#

Pod::Spec.new do |spec|

  spec.name         = "SearchEngine"
  spec.version      = "2.1.0"
  spec.summary      = "Libreria para uso de Proxy y Router de SearchEngineProxy."

  spec.description  = <<-DESC
  Esta librería proporciona un Proxy para conectarse y proporcionar datos de un servidor de ElasticSearch, configurando credenciales para un solo servidor y un solo usuario.
                   DESC

  spec.homepage     = "https://bitbucket.org/jrvillalobosm"

  spec.license      = { :type => 'MIT', :file => 'LICENSE' }

  spec.author       = { 'jorge Villalobos' => 'jorge.r.villalobos.m@gmail.com' }

  spec.platform     = :ios, '9.0'

  spec.source       = { :git => "https://bitbucket.org/jrvillalobosm/ios-network-layer.git", :tag => "SearchEngine_2.1.0" }

  spec.source_files = 'Sources/SearchEngine/**/*.swift'

  spec.requires_arc     = true
  spec.static_framework = true
  spec.dependency 'DatabaseProxy','~> 0.1.0'

end
