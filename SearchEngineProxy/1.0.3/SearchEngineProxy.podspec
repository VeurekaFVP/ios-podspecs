Pod::Spec.new do |s|

  s.name         = "SearchEngineProxy"
  s.version      = "1.0.3"
  s.summary      = "Libreria para uso de Proxy y Router de SearchEngineProxy."

  s.description  = 'Esta librería proporciona un Proxy para conectarse y proporcionar datos de un servidor de ElasticSearch, configurando credenciales para un solo servidor y un solo usuario.'

  s.homepage         = 'http://www.veureka.com/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-elasticsearchproxy.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'
  s.pod_target_xcconfig = { 'SWIFT_VERSION' => '3.0' }

  s.source_files = [
    'Classes/**/*',
  ]

  s.dependency 'CommonsAssets','1.0.3'
  s.dependency 'Logger','1.0.3'
  s.dependency 'AlamofireProxy','1.0.2'

end
