Pod::Spec.new do |s|

  s.name         = "SearchEngineProxy"
  s.version      = "1.0.5"
  s.summary      = "Libreria para uso de Proxy y Router de SearchEngineProxy."

  s.description  = 'Esta librería proporciona un Proxy para conectarse y proporcionar datos de un servidor de ElasticSearch, configurando credenciales para un solo servidor y un solo usuario.'

  s.homepage         = 'http://www.veureka.com/'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ios-elasticsearchproxy.git', :tag => s.version.to_s }

  s.source_files = [
    'Classes/**/*',
  ]

  s.platform         = :ios, '9.0'
  s.requires_arc     = true
  s.static_framework = true
  s.dependency 'BundleManager','1.0.1'
  s.dependency 'CommonsAssets','1.0.3'
  s.dependency 'Logger','1.0.3'
  s.dependency 'AlamofireProxy'

end
