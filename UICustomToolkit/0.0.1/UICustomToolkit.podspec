Pod::Spec.new do |s|

  s.name         = "UICustomToolkit"
  s.version      = "0.0.1"
  s.summary      = "Utilerias para componentes visuales UICustomToolkit."

  s.description  = <<-DESC
CTKFlagPhoneNumber is a phone number textfield that allows you to choose the country code thanks to a picker. It uses libPhoneNumber to format the number in the textfield according to country code.
                   DESC

  s.homepage     = 'http://www.veureka.com/'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/VeurekaFVP/ps-directrips-ios-viajero.git', :tag => s.version.to_s }

  s.ios.deployment_target = '9.0'

  s.source_files = 'Source/UICustomToolkit/**/*.swift'
  s.resource_bundles = {'UICustomToolkit' => ['Source/UICustomToolkit/Resources/**/*'], 'CTKFlagPhoneNumber' => ['Source/UICustomToolkit/CTKFlagPhoneNumber/Resources/**/*']}

  s.dependency 'libPhoneNumber-iOS'
  s.dependency 'RxSwift','3.4.1'
end
