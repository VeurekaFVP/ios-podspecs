Pod::Spec.new do |s|

  s.name         = "UICustomToolkit"
  s.version      = "0.1.0"
  s.summary      = "Utilerias para componentes visuales UICustomToolkit."

  s.description  = <<-DESC
CTKFlagPhoneNumber is a phone number textfield that allows you to choose the country code thanks to a picker. It uses libPhoneNumber to format the number in the textfield according to country code.
CTKMultimediaPicker is a wrapper built around UIImagePickerController and UIDocumentPickerViewController.
                   DESC

  s.homepage     = 'http://www.veureka.com/'
  s.license      = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'jorge Villalobos' => 'jorge.villalobos@societao.com' }
  s.source           = { :git => 'https://bitbucket.org/jrvillalobos/uicustomtoolkit.git', :tag => s.version.to_s }

  s.platform         = :ios, '9.0'
  s.requires_arc     = true
  s.static_framework = true

  s.source_files = 'Source/UICustomToolkit/**/*.swift'
  s.resource_bundles = {
      'UICustomToolkit' => ['Source/UICustomToolkit/Resources/**/*'],
      'CTKFlagPhoneNumber' => ['Source/UICustomToolkit/CTKFlagPhoneNumber/Resources/**/*'],
      'CTKMultimediaPicker' => ['Source/UICustomToolkit/CTKMultimediaPicker/Resources/**/*']
  }

  s.dependency 'libPhoneNumber-iOS'
  s.dependency 'CommonsAssets','~> 2.0.0'

end
