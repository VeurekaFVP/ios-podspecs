Pod::Spec.new do |s|

  s.name         = "UITools"
  s.version      = "0.0.1"
  s.summary      = "Librería para visualizar componentes reusables personalizados o nuevos."

  s.description  = "Esta librería contiene un conjunto de componentes ya sea de la comunidad customizados por nosotros o componentes propios"

  s.homepage     = 'http://www.veureka.com/'


  s.license      = { :type => 'MIT', :file => 'LICENSE' }

  s.author             = { "Tom Thorpe" => "code@tomthorpe.co.uk", 'jorge Villalobos' => 'jorge.villalobos@societao.com' }

  s.platform     = :ios, '7.0'
  s.requires_arc = true

  s.source       = { :git => 'https://bitbucket.org/VeurekaFVP/ios-uitools.git', :tag => s.version.to_s }

  s.source_files = 'Classes/**/*'
  s.exclude_files = "Classes/Exclude"
end
